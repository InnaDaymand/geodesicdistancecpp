//============================================================================
// Name        : TA_placeer_at_key_point_v5p0_partial_version_c.cpp
// Author      : Inna Daymand
// Version     :
// Copyright   : Free
// Description : Geodesic Distance in C++, Ansi-style
//============================================================================


#include <array>
#include <math.h>
#include <chrono>
#include <ctime>
#include <iostream>
#include <vector>
#include <numeric>
#include <string>
#include <functional>
#include "vtkDelimitedTextReader.h"
#include <vtkTable.h>


#include <vtkErrorCode.h>
#include <vtkCellData.h>
#include "TA_placer.h"

TA_placer::TA_placer(std::string iDirectory){
	directory=iDirectory;
	model_image_basename="head_model.nii.gz";
	model_transform_basename="atlas_afine_in_T1_coordsAffine.txt";
	axes_atlas_basename="";
	transformed_axes_filename = directory+ "transformed_" + model_image_basename;
	model_image_filename = directory+ model_image_basename;

	geodesic_weight_array[0]=0.3; geodesic_weight_array[1]=0.2; geodesic_weight_array[2]=0.1;

	layout_2D.push_back({0,0}); layout_2D.push_back({2, 0});
	layout_2D.push_back({-2, 0});layout_2D.push_back({  2, 1});
	layout_2D.push_back({  2, -1});layout_2D.push_back({  0, 1});
	layout_2D.push_back({ 0, -1});layout_2D.push_back({ -2, 1});
	layout_2D.push_back({-2, -1});

	vtkObject::GlobalWarningDisplayOff();

	reader = vtkSmartPointer<vtkNIFTIImageReader>::New();
    reader->SetFileName(model_image_filename.data());
    reader->Update();

    stlWriter = vtkSmartPointer<vtkSTLWriter>::New();
    stlReader =vtkSmartPointer<vtkSTLReader>::New();
	connectedVertices = vtkSmartPointer<vtkIdList> :: New();
	cellIdList = vtkSmartPointer<vtkIdList> :: New();
    vertexDistances = vtkSmartPointer<vtkFloatArray> ::New();
    vertexDistances_for_presentation = vtkFloatArray ::New();
    keyNeighbors = vtkSmartPointer<vtkPoints>::New();
    normal_computer = vtkSmartPointer<vtkPolyDataNormals> ::New();
    obbTree = vtkSmartPointer<vtkOBBTree> ::New();
    points_locator =vtkSmartPointer<vtkPointLocator> ::New();
    reference_points=vtkSmartPointer<vtkPoints> ::New();




	// Generate head surface

	create_model_outer_surface(model_image_filename);

	// read the model
	stlReader->SetFileName(model_stl_filename.data());
	stlReader->Update();

	normal_computer = vtkSmartPointer<vtkPolyDataNormals>:: New();
	normal_computer->SetInputConnection(stlReader->GetOutputPort());
	normal_computer->ComputeCellNormalsOff();
	normal_computer->ComputePointNormalsOn();
	normal_computer->FlipNormalsOff();
	normal_computer->NonManifoldTraversalOn();
	normal_computer->AutoOrientNormalsOn();
	normal_computer->Update();


	model_surface = normal_computer->GetOutput();

	// use an efficient ray tracing algorithm
	// (currently computed on CPU, can be accelerated using GPU with VTK-m)
	obbTree = vtkSmartPointer<vtkOBBTree>::New();
	obbTree->SetDataSet(model_surface);
	obbTree->BuildLocator();

	points_locator = vtkSmartPointer<vtkPointLocator>::New();
	points_locator-> SetDataSet(model_surface);
	points_locator->AutomaticOn();
	points_locator->SetNumberOfPointsPerBucket(10);
	points_locator->BuildLocator();

	// Get global coordinates and a list of reference points
	compute_global_coordinate_from_phantom();
}

TA_placer::~TA_placer(){
	for (int i=0; i<key_points.size();i++){
		delete key_points[i];
	}
//	reader->Delete();
//  stlWriter->Delete();
//  stlReader->Delete();
//	connectedVertices->Delete();
//	cellIdList->Delete();
//    vertexDistances->Delete();
//    vertexDistances_for_presentation->Delete();
//    keyNeighbors->Delete();
//    model_surface->Delete();
//    normal_computer->Delete();
//    obbTree->Delete();
//    points_locator->Delete();
//    reference_points->Delete();
}

// Create an outer surface of of a volumetric model
void TA_placer::create_model_outer_surface (std::string model_filename){

    // read file


    // apply a threshold and create a binary image
    vtkSmartPointer <vtkImageThreshold> threshold = vtkSmartPointer<vtkImageThreshold>::New();
    threshold->SetInputConnection(reader->GetOutputPort());
    threshold->ThresholdByLower(0.5);  // remove all soft tissue
    threshold->ReplaceInOn();
    threshold->SetInValue(0);  // set all values below 400 to 0
    threshold->ReplaceOutOn();
    threshold->SetOutValue(1); // set all values above 400 to 1
    threshold->Update();

    // apply marching cubes algorithm
    vtkSmartPointer <vtkDiscreteMarchingCubes> dmc = vtkSmartPointer<vtkDiscreteMarchingCubes>::New();
    dmc->SetInputConnection(threshold->GetOutputPort());
    dmc->GenerateValues(1, 1, 1);
    dmc->ComputeAdjacentScalarsOff();
    dmc->Update();

    // get largest connected component
    vtkSmartPointer <vtkPolyDataConnectivityFilter> connectivityFilter = vtkSmartPointer<vtkPolyDataConnectivityFilter>::New();
    connectivityFilter->SetInputConnection(dmc->GetOutputPort());
    connectivityFilter->SetExtractionModeToLargestRegion();
    connectivityFilter->Update();

    // smooth surface. This is especially important to reduce geodesic distance error
    vtkSmartPointer <vtkWindowedSincPolyDataFilter>smoother = vtkSmartPointer <vtkWindowedSincPolyDataFilter>::New();
    smoother->SetInputConnection(connectivityFilter->GetOutputPort());
    smoother->SetNumberOfIterations(20);
    smoother->BoundarySmoothingOff();
    smoother->FeatureEdgeSmoothingOff();
    smoother->SetFeatureAngle(30);
    smoother->SetPassBand(0.01);
    smoother->NonManifoldSmoothingOn();
    smoother->NormalizeCoordinatesOn();
    smoother->Update();

    vtkSmartPointer<vtkMatrix4x4> polydata_to_image_matrix = reader->GetQFormMatrix();

    // if model image is not parallel to the axes, create a new image that is parallel to the axes
    is_not_on_axes_flag=is_not_on_axes (polydata_to_image_matrix);

    vtkSmartPointer<vtkTransform> polydata_to_image_transform = vtkSmartPointer<vtkTransform>::New();
    polydata_to_image_transform->SetMatrix(polydata_to_image_matrix);

    vtkSmartPointer<vtkTransformPolyDataFilter> polydata_transform_filter = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
    polydata_transform_filter->SetTransform(polydata_to_image_transform);
    polydata_transform_filter->SetInputConnection(smoother->GetOutputPort());
    polydata_transform_filter->Update();

    // Save surface as STL file


    model_stl_filename = directory+ "head_model.stl";

    stlWriter->SetFileName(model_stl_filename.data());
    stlWriter->SetInputConnection(polydata_transform_filter->GetOutputPort());
    stlWriter->Write();

 //   threshold->Delete();
//    dmc->Delete();
//    connectivityFilter->Delete();
//    smoother->Delete();
//    polydata_to_image_transform->Delete();
 //   polydata_transform_filter->Delete();
}

bool TA_placer::is_not_on_axes(vtkSmartPointer<vtkMatrix4x4> matrix){

        double eps = 0.00000001;

        for (int index1=0; index1 < 3; index1++){
            for(int index2=0; index2 < 3; index2++){
                if ((index1 != index2) and (abs(matrix->GetElement(index1, index2)) > eps))
                    return true;
            }
        }

        return false;
}

// find points that are connected with an edge to the point marked with id
vtkSmartPointer<vtkIdList> TA_placer::get_connected_points(vtkPolyData* mesh, int id){

   connectedVertices->Reset();
   connectedVertices->Initialize();
   cellIdList->Reset();
   cellIdList->Initialize();

   mesh->GetPointCells(id, cellIdList);

   vtkSmartPointer<vtkIdList> pointIdList = vtkSmartPointer<vtkIdList> :: New();
   for (int index=0; index < cellIdList->GetNumberOfIds(); index++){
	   pointIdList->Reset();
       mesh->GetCellPoints(cellIdList->GetId(index), pointIdList);
       if (pointIdList->GetId(0) != id)
           connectedVertices->InsertNextId(pointIdList->GetId(0));
       else
           connectedVertices->InsertNextId(pointIdList->GetId(1));
   }
   return connectedVertices;
}

// compute distance between point p and plane [q, qx, qy]
double get_distance_plane(double* p, double* q, double* q_x, double* q_y){

	double n[3]={0,0,0};
    vtkMath::Cross(q_x, q_y, n);
    double normn=vtkMath::Norm(n);
    n[0] = n[0] / normn;
    n[1] = n[1] / normn;
    n[2] = n[2] / normn;
    double sub[3]={0,0,0};

    vtkMath::Subtract(p, q, sub);
    double distance = abs(vtkMath::Dot(sub, n));

    return distance;
}

void TA_placer::get_points_from_label_image(){

    key_points.clear();

//    using PixelType = short;
//    constexpr unsigned int Dimension = 2;
//    using ImageType = itk::Image<PixelType, Dimension>;
//    using ReaderType = itk::ImageFileReader<ImageType>;

//    ReaderType::Pointer readerITK = ReaderType::New();
//    readerITK->SetFileName(transformed_axes_filename);
    int number_of_labels = 12;//int(np.max( GetArrayFromImage(readerITK->GetOutput())));

    vtkSmartPointer <vtkNIFTIImageReader> reader1=vtkSmartPointer <vtkNIFTIImageReader>::New();
    reader1->SetFileName(transformed_axes_filename.data());
    reader1->Update();
    int err=reader1->GetErrorCode();

    vtkSmartPointer<vtkImageThreshold> threshold = vtkSmartPointer<vtkImageThreshold>::New();
    vtkSmartPointer<vtkDiscreteMarchingCubes> dmc = vtkSmartPointer<vtkDiscreteMarchingCubes>::New();
    vtkSmartPointer<vtkCleanPolyData> cleanPolyData = vtkSmartPointer<vtkCleanPolyData> ::New();
    vtkSmartPointer<vtkCenterOfMass> centerOfMassFilter = vtkSmartPointer<vtkCenterOfMass> ::New();
    vtkSmartPointer<vtkTransformPolyDataFilter> polydata_transform_filter =
    		vtkSmartPointer<vtkTransformPolyDataFilter>::New();
    vtkMatrix4x4* polydata_to_image_matrix = reader1->GetQFormMatrix();
    vtkSmartPointer<vtkTransform> polydata_to_image_transform = vtkSmartPointer<vtkTransform>::New();
    polydata_to_image_transform->SetMatrix(polydata_to_image_matrix);

    for (int label=1; label < number_of_labels + 1; label++){

        threshold->SetInputConnection(reader1->GetOutputPort());
        threshold->ThresholdBetween(label-0.1, label+0.1);  // remove all soft tissue
        threshold->ReplaceInOn();
        threshold->SetInValue(1);  // set all values below 400 to 0
        threshold->ReplaceOutOn();
        threshold->SetOutValue(0);  // set all values above 400 to 1
        threshold->Update();

        dmc->SetInputConnection(threshold->GetOutputPort());
        dmc->GenerateValues(1, 1, 1);
        dmc->Update();

        polydata_transform_filter->SetTransform(polydata_to_image_transform);
        polydata_transform_filter->SetInputConnection(dmc->GetOutputPort());
        polydata_transform_filter->Update();

        cleanPolyData->SetInputConnection(polydata_transform_filter->GetOutputPort());
        cleanPolyData->Update();

        centerOfMassFilter->SetInputConnection(cleanPolyData->GetOutputPort());
        centerOfMassFilter->Update();

        double* center=new double[3];
        double* centerD=( centerOfMassFilter->GetCenter());
        center[0]=centerD[0];center[1]=centerD[1];center[2]=centerD[2];

        key_points.push_back(center);
    }

//    threshold->Delete();
//    dmc->Delete();
//    cleanPolyData->Delete();
//   centerOfMassFilter->Delete();
    //        polydata_to_image_transform->Delete();
    //        polydata_transform_filter->Delete();
}

void TA_placer::compute_global_coordinate_from_phantom(){

	if (is_not_on_axes_flag){
        global_origin = {128, 128, 90};
        vtkMatrix4x4::Identity(global_axes);
    }
    else{
    	// fill vector key_points
        get_points_from_label_image();

        global_origin.Set(key_points[0][0], key_points[0][1], key_points[0][2]);
        vtkVector3d z_axis;
        vtkMath::Subtract(vtkVector3d(key_points[3]).GetData(), vtkVector3d(key_points[0]).GetData(), z_axis.GetData());
        z_axis=z_axis.Normalized();

        vtkVector3d perp_plan_vec1(-1.0, -1.0, (z_axis.GetX()+z_axis.GetY())/z_axis.GetZ());

        vtkVector3d perp_plan_vec2 = z_axis.Cross(perp_plan_vec1);

        vtkVector3d x_axis;
        vtkMath::Subtract(vtkVector3d(key_points[2]).GetData(), vtkVector3d(key_points[0]).GetData(), x_axis.GetData());
        x_axis = project_3d_vector_on_plane(x_axis, perp_plan_vec1, perp_plan_vec2);
        x_axis = x_axis.Normalized();

        vtkVector3d y_axis = z_axis.Cross(x_axis);
        y_axis = y_axis.Normalized();

        global_axes[0]=x_axis.GetX(); global_axes[1]= x_axis.GetY();global_axes[2]= x_axis.GetZ();
        global_axes[3]=y_axis.GetX(); global_axes[4]=y_axis.GetY(); global_axes[5]=y_axis.GetZ();
        global_axes[6]=z_axis.GetX(); global_axes[7]=z_axis.GetY(); global_axes[8]=z_axis.GetZ();
        key_TTF_locations = {key_points[4], key_points[5], key_points[6], key_points[7],
        		key_points[8], key_points[9], key_points[10], key_points[11]};
    }

}

vtkVector3d TA_placer::project_3d_vector_on_plane (vtkVector3d p, vtkVector3d x, vtkVector3d y){
    x = x.Normalized();
    y = y.Normalized();

    vtkVector3d px(x);
    vtkMath::MultiplyScalar(px.GetData(), x.Dot(p));

    vtkVector3d py(y);
    vtkMath::MultiplyScalar(py.GetData(), y.Dot(p));

    vtkVector3d v;
    vtkMath::Add(px.GetData(), py.GetData(), v.GetData());

    if (v.GetX()==0 || v.GetY()==0){
        std::cout <<"Reference point is too close to z-axis, using default axes instead of local ones";
        vtkMath::Add(x.GetData(), y.GetData(), v.GetData());
        double vx=v.GetX();
        v.SetX(v.GetY());
        v.SetY(vx);
    }
    return v;
}

//x 1, 0,0
//y 0, 1, 0

// compute geodesic distance from a given point up to a predefined distance
void TA_placer::compute_geodesic_distances (vtkVector3d reference_point, double max_distance,
		vtkVector3d x_axis, vtkVector3d y_axis
		, double desired_distance, double tolerance){

	vertexDistances->Reset();
	vertexDistances->Initialize();
    vertexDistances->SetNumberOfComponents(1);
    vertexDistances->SetName("geodesic_distance");

    keyNeighbors->Reset();
    keyNeighbors->Initialize();

    // use a 'coloring' mechanism to ensure not repeating the same points
    for (int index=0; index <  model_surface->GetNumberOfPoints(); index++)
        vertexDistances->InsertNextValue(-1.0000);

//    reference_point.Set(10.9,76.7, 70.7);
    int start_point_ID = points_locator->FindClosestPoint(reference_point.GetData());
    vertexDistances->SetValue(start_point_ID, 0.0);

    vtkSmartPointer<vtkIdList>current_iteration_IDs = vtkSmartPointer<vtkIdList>::New();
    current_iteration_IDs->Initialize();
    current_iteration_IDs->InsertNextId(start_point_ID);

    // estimate number of iteration needed to reach the max distance needed

    bool is_first_reference_point = true;
    int index_all = 1;
    double origin[3] = {0, 0, 0};
    vtkSmartPointer<vtkIdList> next_level_list_of_points = vtkSmartPointer<vtkIdList>::New();

    while (current_iteration_IDs->GetNumberOfIds()>0){

        next_level_list_of_points->Reset();
        next_level_list_of_points->Initialize();

        for( int indexes_ID=0; indexes_ID < current_iteration_IDs->GetNumberOfIds(); indexes_ID++){

        	double* point=model_surface->GetPoint(current_iteration_IDs->GetId(indexes_ID));
            reference_point.Set(point[0], point[1], point[2]);
            if (is_first_reference_point){
                origin[0] = reference_point.GetX(), origin[1]=reference_point.GetY(), origin[2]=reference_point.GetZ();
                is_first_reference_point =false;
            }

            double reference_distance = vertexDistances->GetValue(current_iteration_IDs->GetId(indexes_ID));

            get_connected_points(model_surface, current_iteration_IDs->GetId(indexes_ID));

            for (int neighbour_ID_indexes=0;neighbour_ID_indexes< connectedVertices->GetNumberOfIds();
            		neighbour_ID_indexes++ ){

				int current_point_ID = connectedVertices->GetId(neighbour_ID_indexes);

				double* neighbor_point = model_surface->GetPoint(current_point_ID);

				//estimate geodesic distance of neighbour point
				double geodesic_distance_from_original_point = reference_distance +
						sqrt(vtkMath::Distance2BetweenPoints(neighbor_point, reference_point.GetData()));

//                    if smaller distance exists, update to find shortest path
				if (vertexDistances->GetValue(current_point_ID) > geodesic_distance_from_original_point){
					// update neighbour's geodesic distance value
					vertexDistances->SetValue(current_point_ID, geodesic_distance_from_original_point);
				}

//                     if this point was not visited before (we pre-set all to -1)
				if (vertexDistances->GetValue(current_point_ID) < 0){
					// update neighbour's geodesic distance value
					vertexDistances->SetValue(current_point_ID, geodesic_distance_from_original_point);

					if (geodesic_distance_from_original_point < max_distance){
						// mark as a neighbour to compute its geodesic distance in next iteration
						next_level_list_of_points->InsertNextId(current_point_ID);
					}
				}

//                     if the point is at a distance that is similar to a key point
				if (abs(desired_distance - geodesic_distance_from_original_point) < tolerance){

//                             if the point is on the axial plane
						double distance = get_distance_plane(neighbor_point, origin, x_axis.GetData(), y_axis.GetData());
						if (distance < tolerance){
							keyNeighbors->InsertNextPoint(neighbor_point);
						}
				}
            }
        }

        current_iteration_IDs->Reset();
        current_iteration_IDs->DeepCopy(next_level_list_of_points);

        index_all = index_all + 1;
    }
}

// normalize the geodesic distances for visualization
void TA_placer::normalize_for_presentation(){

	vertexDistances_for_presentation->Reset();
	vertexDistances_for_presentation->Initialize();
    vertexDistances_for_presentation->SetNumberOfComponents(1);
    vertexDistances_for_presentation->SetName("geodesic_distance");

    double max_value = -1.0;
    for(int current_point_ID=0;current_point_ID< vertexDistances->GetNumberOfTuples(); current_point_ID++){
        if (vertexDistances->GetValue(current_point_ID) > max_value){
            max_value = vertexDistances->GetValue(current_point_ID);
        }
    }


    for(int current_point_ID=0; current_point_ID< vertexDistances->GetNumberOfTuples(); current_point_ID++){
        if (vertexDistances->GetValue(current_point_ID) < 0){ // if not computed it is because it is too far make it blue
            vertexDistances_for_presentation->InsertNextValue(1.0);
        }
        else{
            vertexDistances_for_presentation->InsertNextValue(vertexDistances->GetValue(current_point_ID)/max_value);
        }
    }

}

// present the model with the compute distances on it.
void TA_placer::present_mesh_and_computed_distances(){

    vtkPoints* points = model_surface->GetPoints();

    // Add distances to each point
    normalize_for_presentation();

    vtkPolyData* polyData = vtkPolyData::New();
    polyData->SetPoints(points);
    polyData->GetPointData()->SetScalars(vertexDistances_for_presentation);

    vtkSmartPointer<vtkVertexGlyphFilter> vertexGlyphFilter = vtkSmartPointer<vtkVertexGlyphFilter> ::New();
    vertexGlyphFilter->SetInputData(polyData);
    vertexGlyphFilter->Update();

    vtkSmartPointer<vtkPolyDataMapper>signedDistanceMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    signedDistanceMapper->SetInputConnection(vertexGlyphFilter->GetOutputPort());
    signedDistanceMapper->ScalarVisibilityOn();

    vtkSmartPointer<vtkActor>signedDistanceActor = vtkSmartPointer<vtkActor>::New();
    signedDistanceActor->SetMapper(signedDistanceMapper);

    vtkSmartPointer<vtkRenderer>renderer = vtkSmartPointer<vtkRenderer>::New();
    renderer->AddViewProp(signedDistanceActor);

    vtkSmartPointer<vtkRenderWindow>renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
    renderWindow->AddRenderer(renderer);

	vtkSmartPointer<vtkRenderWindowInteractor>renWinInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
    renWinInteractor->SetRenderWindow(renderWindow);

    renderWindow->Render();
    renWinInteractor->Start();
}

double TA_placer::compute_max_distance_to_compute(){

    double min_vals=10000;
    double max_vals=-10000;
    for(int i=0; i<8; i++){
    	if(fmin(layout_2D[i][0], layout_2D[i][1])<min_vals){
    		min_vals=fmin(layout_2D[i][0], layout_2D[i][1]);
    	}
    	if(fmax(layout_2D[i][0], layout_2D[i][1])>max_vals){
    		max_vals=fmax(layout_2D[i][0], layout_2D[i][1]);
    	}
    }

    return (max_vals - min_vals)*1.25;

}

void TA_placer::statistics(char clayout_code){
	//for statistics
	double mean=0;
	double maxValue=-1000000;
	double minValue=1000000;
	double variance=0;
	double standardDeviation;
	bool first=true;

	vtkSmartPointer<vtkDelimitedTextReader> readerCSV=vtkSmartPointer<vtkDelimitedTextReader>::New();
	std::string csv_file_name=directory+"result_"+clayout_code+".csv";
	readerCSV->SetFileName(csv_file_name.data());
	readerCSV->DetectNumericColumnsOn();
	readerCSV->SetFieldDelimiterCharacters(",");//comma is the separator
	readerCSV->Update();

	int err=readerCSV->GetErrorCode();

	vtkTable* table=readerCSV->GetOutput();
	std::vector<double> data;

	int count_row=table->GetNumberOfRows();
	int count_col=table->GetNumberOfColumns();
	for(int i=0; i < vertexDistances_for_presentation->GetNumberOfValues();i++){
		double current_value=vertexDistances_for_presentation->GetValue(i);
		double current_value_csv=table->GetValue(i, 0).ToDouble();
		double diff_value=abs(current_value - current_value_csv);
		data.push_back(diff_value);
		if(minValue > diff_value && current_value!=0)
			minValue=diff_value;
		if(maxValue < diff_value && current_value<1)
			maxValue=diff_value;
		if(first){
			mean=diff_value;
			first=false;
			continue;
		}
		double mean_next=mean+ ( diff_value - mean)/i;
		double variance_next=variance + (diff_value - mean)*(diff_value - mean_next);
		mean=mean_next;
		variance=variance_next;
	}
	if(variance !=0)
		standardDeviation=std::sqrt(variance/(vertexDistances_for_presentation->GetNumberOfValues()-1));
	std::cout << "min value:" + std::to_string(minValue)+"\n";
	std::cout << "max value:" + std::to_string(maxValue)+"\n";
	std::cout << "mean value:" + std::to_string(mean)+"\n";
	std::cout << "standard deviation value:" + std::to_string(standardDeviation)+"\n";

	mean=std::accumulate(data.begin(), data.end(), 0.0)/data.size();
	standardDeviation=std::inner_product(data.begin(), data.end(), data.begin(), 0.0,
			[](double const& x, double const& y){return x+y;},
			[mean](double const& x, double const& y){ return (x-mean)*(y-mean);});
	standardDeviation=std::sqrt(standardDeviation/(vertexDistances_for_presentation->GetNumberOfValues()-1));
	std::cout << "calc stl \n";
	std::cout << "mean value:" + std::to_string(mean)+"\n";
	std::cout << "standard deviation value:" + std::to_string(standardDeviation)+"\n";

}

void TA_placer::place_TA (vtkVector3d reference_point, double* angle_degs, char clayout_code,
              double gel_size_above_skin, double ceramic_size, double plate_size,
			  bool computeGelHeight = true){


    // compute geodesic distances:

    // compute bounds to save time of geodesic computing
	std::chrono::time_point<std::chrono::system_clock> start, end;
    double distance_between_key_central_and_key_TAs = fmax (layout_2D[1][0] - layout_2D[0][0]
														, layout_2D[1][1]-layout_2D[0][1]);
    double max_distance = compute_max_distance_to_compute()*22.0;

	start = std::chrono::system_clock::now();
    compute_geodesic_distances( reference_point,max_distance, vtkVector3d(global_axes[0], global_axes[1], global_axes[2]),
    		vtkVector3d(global_axes[3], global_axes[4], global_axes[5]),
			distance_between_key_central_and_key_TAs*22.0, 0.5);
	end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds;
    elapsed_seconds = end - start;
    std::cout << "geodesic distance time: " << elapsed_seconds.count() << "s\n";
    // For debug:
    present_mesh_and_computed_distances();
//    statistics(clayout_code);
    //print output for tests
    /*std::cout << "\n";
    int size_output=vertexDistances_for_presentation->GetNumberOfValues();
    for(int i=0, count=0, size=0; i < size_output && size <= 200 ; i++){
    	double val=vertexDistances_for_presentation->GetValue(i);
    	if(val!=1.0){
    		std::cout << "index: "+std::to_string(i);
    		std::cout << " value="+std::to_string(val)+ " ";
    		size++;
    		count++;
			if(count >10){
				count=0;
				std::cout << "\n";
			}
    	}
    }*/
    place_spheres_on_points(directory+ "Intersection_points_" + layout_code + ".stl");
}

void TA_placer::place_spheres_on_points(std::string output_filename){

    vtkSmartPointer<vtkAppendPolyData> all_spheres = vtkSmartPointer<vtkAppendPolyData>::New();
    all_spheres->SetNumberOfInputs(keyNeighbors->GetNumberOfPoints());
    vtkSmartPointer<vtkSphereSource> source=vtkSmartPointer<vtkSphereSource>::New();
    vtkSmartPointer<vtkPolyData> input=vtkSmartPointer<vtkPolyData>::New();
    for(int pointID=0; pointID <  keyNeighbors->GetNumberOfPoints(); pointID++){

        source->SetCenter(keyNeighbors->GetPoint(pointID));
        source->SetRadius(1);
        source->Update();

        input->ShallowCopy(source->GetOutput());

        all_spheres->AddInputData(input);
    }

    all_spheres->Update();

    stlWriter->SetFileName(output_filename.data());
    stlWriter->SetInputConnection(all_spheres->GetOutputPort());
    stlWriter->Write();
}



void TA_placer::TA_placer_object_place_multiple_TAs( double* angles_of_layouts,
						double gel_size_above_skin, double ceramic_size,
                           double plate_size,
						   bool computeGelHeight){

        if (reference_points->GetNumberOfPoints() < 1){
        	for(int i=0; i< key_TTF_locations.size(); i++)
        		reference_points->InsertNextPoint (key_TTF_locations[i]);
        }
    	std::chrono::time_point<std::chrono::system_clock> start, end;

        // Loop over reference points
        for (int index=0; index < reference_points->GetNumberOfPoints(); index++){
            vtkVector3d reference_point(reference_points->GetPoint(index));

            // For debug - create an axial plane
            // intersection = get_points_at_plane(self.model_filename, reference_point, self.global_axes[0], self.global_axes[1])
            std::cout << "\n";
            std::cout << "reference point "+std::to_string(index) + " ("+
            		std::to_string(reference_point[0])+", "+
            		std::to_string(reference_point[1])+", "+
            		std::to_string(reference_point[2])+") " << "\n";

        	start = std::chrono::system_clock::now();
        	place_TA(reference_point, angles_of_layouts, layout_code[index],
        	              gel_size_above_skin, ceramic_size, plate_size,
        				  computeGelHeight);
           	end = std::chrono::system_clock::now();
        }
}



