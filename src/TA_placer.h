//============================================================================
// Name        : TA_placer.cpp
// Author      : Inna Daymand
// Version     :
// Copyright   : Free
// Description : Geodesic Distance in C++, Ansi-style
//============================================================================

#include <vtkActor.h>
#include <vtkCamera.h>
#include <vtkDiscreteMarchingCubes.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkNIFTIImageReader.h>
#include <vtkImageThreshold.h>
#include <vtkPolyDataConnectivityFilter.h>
#include <vtkWindowedSincPolyDataFilter.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkSTLWriter.h>
#include <vtkSTLReader.h>
#include <vtkIDList.h>
#include <vtkMath.h>
#include <vtkFloatArray.h>
#include <vtkPolyDataNormals.h>
#include <vtkOBBTree.h>
#include <vtkPointLocator.h>
#include <vtkPointData.h>
#include <vtkVertexGlyphFilter.h>
#include <vtkRenderer.h>
#include <vtkAppendPolyData.h>
#include <vtkSphereSource.h>
#include <vtkCleanPolyData.h>
#include <vtkCenterOfMass.h>
#include <vtkMatrix4x4.h>
#include <vtkVector.h>

#include <vector>

class TA_placer{

	vtkSmartPointer <vtkNIFTIImageReader> reader;
    vtkSmartPointer<vtkSTLWriter>stlWriter;
    vtkSmartPointer<vtkSTLReader>stlReader ;
	vtkSmartPointer<vtkIdList> connectedVertices;
	vtkSmartPointer<vtkIdList> cellIdList;
    vtkSmartPointer<vtkFloatArray> vertexDistances;
    vtkFloatArray* vertexDistances_for_presentation;
    vtkSmartPointer<vtkPoints> keyNeighbors;
    vtkPolyData* model_surface;
    vtkSmartPointer<vtkPolyDataNormals> normal_computer;
    vtkSmartPointer<vtkOBBTree> obbTree;
    vtkSmartPointer<vtkPointLocator> points_locator;
    vtkSmartPointer<vtkPoints> reference_points;

    std::string directory;
    std::string model_image_basename;
    std::string model_stl_basename;
    std::string model_image_filename;
    std::string model_stl_filename;
    std::string model_transform_basename;
    std::string axes_atlas_basename;
    std::string transformed_axes_filename;


    vtkVector3d global_origin;
    double global_axes[9];

//    double* names_code;
//    double* distance_errors;
//    double* angular_errors;
//    double* gel_distance_errors;
//   double* computation_times;
 //   double* last_TA;

    double geodesic_weight_array[3];
    bool is_not_on_axes_flag;

    std::vector <double*> key_points;
    std::vector <double*> key_TTF_locations;
	std::vector<std::array<double, 2>> layout_2D;

	char layout_code[8]= {'1', '2', '3', '4', '5', '6', '7', '8'};


protected:

// Create an outer surface of of a volumetric model
void create_model_outer_surface (std::string model_filename);

vtkSmartPointer<vtkIdList> get_connected_points(vtkPolyData* mesh, int id);
	
void compute_geodesic_distances (vtkVector3d reference_point, double max_distance, vtkVector3d x_axis, vtkVector3d y_axis
		, double desired_distance, double tolerance = 1.0);

void normalize_for_presentation();

void present_mesh_and_computed_distances();

void compute_global_coordinate_from_phantom();

void get_points_from_label_image();

bool is_not_on_axes(vtkSmartPointer<vtkMatrix4x4> matrix);

vtkVector3d project_3d_vector_on_plane (vtkVector3d p, vtkVector3d x, vtkVector3d y);

void place_TA(vtkVector3d reference_point,  double* angle_degs, char layout_code,
              double gel_size_above_skin, double ceramic_size, double plate_size,
			  bool computeGelHeight);

double compute_max_distance_to_compute();

void place_spheres_on_points(std::string output_filename);
void statistics(char clayout_code);

public:
TA_placer(std::string iDirectory);
virtual ~TA_placer();
void TA_placer_object_place_multiple_TAs( double* angles_of_layouts,
						double gel_size_above_skin, double ceramic_size,
                           double plate_size,
						   bool computeGelHeight=true);

};
