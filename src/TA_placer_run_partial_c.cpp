//============================================================================
// Name        : TA_placer_run_partial_c.cpp
// Author      : Inna Daymand
// Version     :
// Copyright   : Free
// Description : Geodesic Distance in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <array>
#include "TA_placer.h"

using namespace std;


string directory = "/Users/idaymand/workspace/GeodesicDistance/Data/";

double  angles_of_layouts_all[]={0,0,0,0,0,0,0,0};


double gel_size_above_skin = 1;
double ceramic_size = 1;
double plate_size = 1;
double geodesic_weight_array[]={0.3, 0.2, 0.1};

int main(int argc, char *argv[]) {
	TA_placer* object = new TA_placer(directory);


	object->TA_placer_object_place_multiple_TAs( angles_of_layouts_all,
			gel_size_above_skin,
			ceramic_size,
			plate_size,true);
	return 0;
}
